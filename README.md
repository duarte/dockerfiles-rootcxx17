# ROOT compiled with C++17

Build ROOT with the C=++17 standard instead of the default C++14
of the pre-compiled images on 
[root-download page](https://root.cern.ch/downloading-root).


## Installation
Assuming ```docker``` is installed on your system (host-computer), 
you can either:
 *  Download the automated build from the gitlab (**Recommended**): 
 ```bash
 $ docker pull gitlab-registry.cern.ch/duarte/dockerfiles-rootcxx17/root:6.24.06
 ```
 * Alternativelly you can build the image from the [Dockerfile](Dockerfile)
   * Directly from the github repository
   ```bash
   # Using the docker from the repository (no downloading the repo)
   $ docker build -t gitlab-registry.cern.ch/duarte/dockerfiles-rootcxx17/root:6.24.06 .
   ```
   * Downloading this repo and building the image:
   ```bash
   $ git clone https://github.com/duartej/dockerfiles-rootcxx17
   $ cd dockerfiles-rootcxx17/
   $ docker-compose build -t root:6.24.06 .
   ```
Note that every time a new tag is created, the image will be created automatically
in the registry.

## Usage
Run the container as 
```bash
$ docker run --rm -i \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -e DISPLAY=unix${DISPLAY} \
    gitlab-registry.cern.ch/duarte/dockerfiles-rootcxx17/root:6.24.06
```
If you built the image using a different name, subtitute it there.

Don't forget to allow the docker group to use the X-server if you need to use 
the X-windows:
```bash
xhost +local:docker
```



