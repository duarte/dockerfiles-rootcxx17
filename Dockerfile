# 
# ROOT with CXX17
# https://github.com/duartej/dockerfiles-rootpython3
#
# Build ROOT  CXX17
# 
# Build the image: docker build -t gitlab-registry.cern.ch/duarte/dockerfiles-rootcxx17:root:6.24.06 .
# docker build -t duartej/rootcxx17
FROM phusion/baseimage:focal-1.2.0
LABEL author="jorge.duarte.campderros@cern.ch" \ 
    version="8.0" \ 
    description="Docker image for ROOT with python3 support, compiled with CXX17 "

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# -- Update and get needed packages
USER root

RUN apt-get update \
  && install_clean --no-install-recommends software-properties-common \ 
  build-essential \
  python3 \
  python3-dev \ 
  python3-numpy \
  python3-scipy \ 
  libx11-dev \ 
  libxpm4 \ 
  libxpm-dev \ 
  libxft2 \ 
  libxft-dev \
  libxpm-dev \ 
  libxext-dev \
  libpng16-16 \
  libjpeg8 \ 
  libtiff5 \ 
  libpcre3-dev \
  libssl-dev \ 
  xlibmesa-glu-dev \ 
  libglew-dev \ 
  libmysqlclient-dev \ 
  libgraphviz-dev \ 
  libavahi-compat-libdnssd-dev \ 
  libxml2-dev \ 
  libkrb5-dev \ 
  #libqwt5-dev \ 
  cmake \ 
  git \ 
  curl

ARG ROOT_VERSION=6.24.06

ENV ROOTSYS="/opt/root6"
ENV PATH="$ROOTSYS/bin:$PATH"
ENV LD_LIBRARY_PATH="$ROOTSYS/lib:$LD_LIBRARY_PATH"
ENV LIBPATH="$ROOTSYS/lib:$LIBPATH"
ENV PYTHONPATH="$ROOTSYS/lib:$PYTHONPATH"
ENV CMAKE_PREFIX_PATH="$ROOTSYS:$CMAKE_PREFIX_PATH"

RUN mkdir -p ${ROOTSYS}/src && mkdir -p ${ROOTSYS}/build && \
    curl -o ${ROOTSYS}/root.${ROOT_VERSION}.tar.gz \
            https://root.cern.ch/download/root_v${ROOT_VERSION}.source.tar.gz && \
    tar zxf ${ROOTSYS}/root.${ROOT_VERSION}.tar.gz -C ${ROOTSYS}/src && \
    rm -f ${ROOTSYS}/root.${ROOT_VERSION}.tar.gz && \
    cd ${ROOTSYS}/build && \
    cmake -Dgdml=ON \
           -Dgenvector=ON \
           -Dmathmore=ON \
           -Dminuit2=ON \
           -Dthread=ON \
           -Dx11=ON \
           -Dopengl=ON \
           -Dtmva=OFF -Dtmva-cpu=OFF -Dtmva-pymva=OFF \
           -Dhttp=OFF \
           -Dwebgui=OFF \
           -Droot7=OFF \
           -Dfftw3=OFF \
           -Dfitsio=OFF \
           -Dclad=OFF \
           -Dspectrum=OFF \
           -Dvdt=OFF \
           -Dxrootd=OFF \
           -Droofit=OFF \
           -Ddataframe=OFF \
           -Dpython3=ON \
           -DPYTHON_EXECUTABLE=/usr/bin/python3 \
           -DCMAKE_INSTALL_PREFIX=../ \
           -DCMAKE_CXX_STANDARD=17 \
           ../src/root-${ROOT_VERSION} && \
    make -j`grep -c processor /proc/cpuinfo` && \
    make install && \
    rm -rf ${ROOTSYS}/{src,build}


CMD ["/bin/bash","-i"]
